﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;

public partial class UpdateUser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null)
            Response.Redirect("nopermission.aspx");
    }
    protected void Update_Click(object sender, EventArgs e)
    {
        OleDbConnection con1 = new OleDbConnection();
        con1.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
        con1.Open();

        bool flag = false;

        string sqlstring = "UPDATE MyUsers SET  ";
        if (Fn.Text != "")
        {
            flag = true;
            sqlstring += "MyFirstname='" + Fn.Text + "' ";
        }
        if (Ln.Text != "")
        {
            if (flag)
                sqlstring += ",MyLastname='" + Fn.Text + "' ";
            else
            {
                flag = true;
                sqlstring += "MyLastname='" + Fn.Text + "' ";
            }
        }
        if (RadioButtonList3.SelectedValue.ToString() != "")
        {
            if (flag)
                sqlstring += ",Admin='" + RadioButtonList3.SelectedValue.ToString() + "' ";
            else
            {
                sqlstring += "Admin='" + RadioButtonList3.SelectedValue.ToString() + "' ";
                flag = true;
            }
        }

        if (RadioButtonList4.SelectedValue.ToString() != "")
        {
            if (flag)
                sqlstring += ",Seller='" + RadioButtonList4.SelectedValue.ToString() + "' ";
            else
            {
                sqlstring += "Seller='" + RadioButtonList4.SelectedValue.ToString() + "' ";
                flag = true;
            }
        }

        sqlstring += "where MyEmail = '" + Em.Text + "'";
        OleDbCommand conSer = new OleDbCommand(sqlstring, con1);
        int Check = 0;
        Check = conSer.ExecuteNonQuery();
        if (Check == 0)
        {
            Response.Write("Invalid user");
        }
        else
        {
            Response.Write("updated " + Check + " users");

        }

        con1.Close();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}