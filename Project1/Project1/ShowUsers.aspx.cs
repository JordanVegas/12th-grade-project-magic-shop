﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;

public partial class ShowUsers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null)
            Response.Redirect("nopermission.aspx");

        if (!IsPostBack)
        {
            OleDbConnection con1 = new OleDbConnection();
            con1.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
            con1.Open();
            string sqlstring = "select * FROM MyUsers  Order By myfirstname asc";
            OleDbCommand conSer = new OleDbCommand(sqlstring, con1);
            OleDbDataReader Drdr = conSer.ExecuteReader();
            Repeaterofusers.DataSource = Drdr;
            Repeaterofusers.DataBind();
            con1.Close();
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}