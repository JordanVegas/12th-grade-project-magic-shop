﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Order.aspx.cs" Inherits="Order" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body >
    <form id="form1" runat="server">
        <center>
    <div>
        
    <table style="border-spacing: 5px;">
        <tr>
            <td>
 <asp:Label ID="h" runat="server" Text="Full name Of the owner of the payment method"></asp:Label>
                
            </td>
            
        </tr>
         <tr>
            <td>
 
 <asp:TextBox ID="OwnerName" runat="server" ></asp:TextBox> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="OwnerName" ErrorMessage="Full Name Is Missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>

        </tr>
         <tr>
            <td>
                <asp:Label ID="a" runat="server" Text="Credit Card Number"></asp:Label>
            </td>

        </tr>
         <tr>
            <td>
 <asp:TextBox ID="CCnum" runat="server" ></asp:TextBox> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="CCnum" ErrorMessage="Credit Card Number Is Missing" ForeColor="Red"></asp:RequiredFieldValidator>
               

            </td>

        </tr>
        <tr>
            <td>
                 <asp:Label ID="sdf" runat="server" Text="Credit Card Expiry Month"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="CCdate1" runat="server"> </asp:TextBox>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="CCdate1" ErrorMessage="Credit Card Expiry Year is missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr>
            <td>
                 <asp:Label ID="Label4" runat="server" Text="Credit Card Expiry Year"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="CCdate2" runat="server"> </asp:TextBox>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="CCdate2" ErrorMessage="Credit Card Expiry Year is missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Credit Card CVV"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
 <asp:TextBox ID="CVC" runat="server"> </asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="CVC" ErrorMessage="Credit Card CVV is missing" ForeColor="Red"></asp:RequiredFieldValidator>
              

            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Address"></asp:Label>
            </td>
            </tr>
        <tr>
            <td>
<asp:TextBox ID="Address1" runat="server"> </asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="Address1" ErrorMessage="Address is missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Country"></asp:Label>
            </td>
            </tr>
        <tr>
            <td>
<asp:TextBox ID="Country1" runat="server"> </asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="Country1" ErrorMessage="Country is missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
                <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Postal Code"></asp:Label>
            </td>
            </tr>
        <tr>
            <td>
<asp:TextBox ID="Postal1" runat="server"> </asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="Postal1" ErrorMessage="Postal Code is missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>

                <tr>
            <td>
                <asp:Label ID="Label5" runat="server" Text="City"></asp:Label>
            </td>
            </tr>
        <tr>
            <td>
<asp:TextBox ID="City1" runat="server"> </asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="City1" ErrorMessage="City is missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>






         <tr>
            <td>
<asp:Button ID="Button1" runat="server" Text="Place Order" OnClick="Button1_Click" />
&nbsp
                 <asp:Button ID="Button2" runat="server" Text="Clear" OnClick="Button2_Click" CausesValidation="False"/>
             </td>
             
        </tr>
         
    </table>
    </div>
       
        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" CausesValidation="False">Homepage</asp:LinkButton>
    </center>
            </form>
</body>
</html>
