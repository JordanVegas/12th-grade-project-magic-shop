﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <center>
        <h1>Home Page</h1>    
        <asp:LinkButton ID="Login" runat="server" OnClick="Login_Click">Login</asp:LinkButton><br /><br />
        <asp:LinkButton ID="Logout" runat="server" OnClick="Logout_Click">Logout</asp:LinkButton><br /><br />
        <asp:LinkButton ID="Register" runat="server" OnClick="Register_Click">Register</asp:LinkButton><br /><br />
        <asp:LinkButton ID="Items" runat="server" OnClick="Items_Click">Show all Items</asp:LinkButton><br /><br />
        <asp:LinkButton ID="ShowBasket" runat="server" OnClick="Basket_Click">Show Basket</asp:LinkButton><br /><br />
        </center>
    </form>
</body>
</html>
