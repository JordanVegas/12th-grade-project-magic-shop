﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Admin.aspx.cs" Inherits="Admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="font-size:large;">
    <form id="form1" runat="server">
        <center>
        <h1>Admin Home Page</h1>    
        <asp:LinkButton ID="Update" runat="server" OnClick="Update_Click">Update user</asp:LinkButton><br /><br />
        <asp:LinkButton ID="Delete" runat="server" OnClick="Delete_Click">Delete user</asp:LinkButton><br /><br />
        <asp:LinkButton ID="Display" runat="server" OnClick="Display_Click">Show all users</asp:LinkButton><br /><br />
        <asp:LinkButton ID="ShowItems" runat="server" OnClick="Show_Items_Click">List Items</asp:LinkButton><br /><br />
        <asp:LinkButton ID="AddItem" runat="server" OnClick="add_Click">Add Item</asp:LinkButton><br /><br />
        <asp:LinkButton ID="Home" runat="server" OnClick="Home_Click">HomePage</asp:LinkButton><br /><br />
        </center>
    </form>
</body>
</html>
