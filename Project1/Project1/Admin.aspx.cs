﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Update_Click(object sender, EventArgs e)
    {
        Response.Redirect("UpdateUser.aspx");
    }
    protected void Delete_Click(object sender, EventArgs e)
    {
        Response.Redirect("DeleteUser.aspx");
    }
    protected void Display_Click(object sender, EventArgs e)
    {
        Response.Redirect("ShowUsers.aspx");
    }
    protected void Show_Items_Click(object sender, EventArgs e)
    {
        Response.Redirect("ShowItems.aspx");
    }


    protected void add_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddItem.aspx");
    }
    protected void Home_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}