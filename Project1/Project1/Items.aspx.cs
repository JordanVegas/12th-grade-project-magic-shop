﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class Items : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            OleDbConnection con4 = new OleDbConnection();
            con4.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
            con4.Open();
            string sqlstring = "select * from MyItems where Stock <> '0' ORDER BY ItemName ";
            OleDbCommand ConSer = new OleDbCommand(sqlstring, con4);
            OleDbDataReader Drdr4 = ConSer.ExecuteReader();
            Shop.DataSource = Drdr4;
            Shop.DataBind();
            


            basket b = new basket();
            Session["basket"] = b;
            con4.Close();
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        OleDbConnection con4 = new OleDbConnection();
        con4.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
        con4.Open();
        string sqlstring = "select * from MyItems where Category='Cards' AND Stock <> '0' ORDER BY ItemName";
        OleDbCommand ConSer = new OleDbCommand(sqlstring, con4);
        OleDbDataReader Drdr4 = ConSer.ExecuteReader();
        Shop.DataSource = Drdr4;
        Shop.DataBind();
        con4.Close();
    }
    protected void Button20_Click(object sender, EventArgs e)
    {
        OleDbConnection con4 = new OleDbConnection();
        con4.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
        con4.Open();
        string sqlstring = "select * from MyItems where Stock <> '0' ORDER BY ItemName";
        OleDbCommand ConSer = new OleDbCommand(sqlstring, con4);
        OleDbDataReader Drdr4 = ConSer.ExecuteReader();
        Shop.DataSource = Drdr4;
        Shop.DataBind();
        con4.Close();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        OleDbConnection con4 = new OleDbConnection();
        con4.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
        con4.Open();
        string sqlstring = "select * from MyItems where category='ITR' AND Stock <> '0' ORDER BY ItemName";
        OleDbCommand ConSer = new OleDbCommand(sqlstring, con4);
        OleDbDataReader Drdr4 = ConSer.ExecuteReader();
        Shop.DataSource = Drdr4;
        Shop.DataBind();
        con4.Close();
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        OleDbConnection con4 = new OleDbConnection();
        con4.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
        con4.Open();
        string sqlstring = "select * from MyItems where Category='Gimmicks' AND Stock <> '0' ORDER BY ItemName";
        OleDbCommand ConSer = new OleDbCommand(sqlstring, con4);
        OleDbDataReader Drdr4 = ConSer.ExecuteReader();
        Shop.DataSource = Drdr4;
        Shop.DataBind();
        con4.Close();
    }

    protected void Button4_Click(object sender, EventArgs e)
    {
        OleDbConnection con4 = new OleDbConnection();
        con4.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
        con4.Open();
        string sqlstring = "select * from MyItems where Category='General' AND Stock <> '0' ORDER BY ItemName";
        OleDbCommand ConSer = new OleDbCommand(sqlstring, con4);
        OleDbDataReader Drdr4 = ConSer.ExecuteReader();
        Shop.DataSource = Drdr4;
        Shop.DataBind();
        con4.Close();
    }



    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("default.aspx");
    }

    protected void shop_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "AddToBasket")
        {
                Shop.SelectedIndex = e.Item.ItemIndex;

                basket b = new basket();
            b = (basket)Session["basket"];
            Item i = new Item();
            //i.ItemCode = ((Label)Shop.SelectedItem.FindControl("Label8")).Text;
            i.ItemCode = Shop.DataKeys[e.Item.ItemIndex].ToString();
            b.addItem(i);
            Session["basket"] = b;

            if (Session["basket"] != null)
                Response.Write("SUCCESSSSS LOLLLLLL");
            else Response.Write("no");
        }
    }

    protected void Shop_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void Basket_Click(object sender, EventArgs e)
    {
        Response.Redirect("ShowBasket.aspx");
    }
}