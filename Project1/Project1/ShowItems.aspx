﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowItems.aspx.cs" Inherits="ShowItems" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <center>
        <div>
            <table border="1">
                    <tr>
                    <td style="background-color:gainsboro; height: 18px;"><asp:Label ID="Label1" runat="server" Text="Item Name" ForeColor="Black"></asp:Label></td>
                    <td style="background-color:red; height: 18px;"><asp:Label ID="Label2" runat="server" Text="Description" ForeColor="White"></asp:Label></td>
                    <td style="background-color:gainsboro; height: 18px;"><asp:Label ID="Label3" runat="server" Text="Price" ForeColor="Black"></asp:Label></td>
                    <td style="background-color:red; height: 18px;"><asp:Label ID="Label4" runat="server" Text="Category" ForeColor="White"></asp:Label></td>
                    <td style="background-color:gainsboro; height: 18px;"><asp:Label ID="Label5" runat="server" Text="Creator" ForeColor="Black"></asp:Label></td>
                    <td style="background-color:red; height: 18px;"><asp:Label ID="Label6" runat="server" Text="Uses" ForeColor="White"></asp:Label></td>
                    <td style="background-color:gainsboro; height: 18px;"><asp:Label ID="Label7" runat="server" Text="Email" ForeColor="Black"></asp:Label></td>
                    <td style="background-color:red; height: 18px;"><asp:Label ID="Label8" runat="server" Text="Stock" ForeColor="White"></asp:Label></td>
                    </tr>
                <asp:Repeater ID="Repeaterofusers" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%#DataBinder.Eval(Container.DataItem, "ItemName") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "Description") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "price") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "category") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "creator") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "Uses") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "Email") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "Stock") %></td>
                                    <td><a href="StockUpdate.aspx?ID=<%#DataBinder.Eval(Container.DataItem, "Code")%>"><asp:Label ID="Label8" runat="server" Text="Update Stock" ForeColor="LimeGreen"></asp:Label></a></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
            </table>
            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" Text="Go Back">לדף הבית</asp:LinkButton>
        </div>
            </center>
    </form>
</body>
</html>
