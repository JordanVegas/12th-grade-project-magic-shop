﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["admin"] != null)
            Response.Write("Hello Admin, Please click <a href='Admin.aspx'> here </a>to go to the Admins zone");
        if (Session["seller"] != null)
            Response.Write("Hello Seller, Please click <a href='Seller.aspx'> here </a>to go to the Sellers zone");
    }
    protected void Login_Click(object sender, EventArgs e)
    {
        Response.Redirect("Login.aspx");
    }
    protected void Register_Click(object sender, EventArgs e)
    {
        Response.Redirect("Register.aspx");
    }

    protected void Items_Click(object sender, EventArgs e)
    {
        Response.Redirect("Items.aspx");
    }
    protected void Basket_Click(object sender, EventArgs e)
    {
        Response.Redirect("ShowBasket.aspx");
    }
    protected void Logout_Click(object sender, EventArgs e)
    {
        Response.Redirect("Logout.aspx");
    }
}