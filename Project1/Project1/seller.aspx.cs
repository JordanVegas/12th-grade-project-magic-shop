﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class seller : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["seller"] == null)
        {
            Response.Redirect("nopermission.aspx");
        }
    }
    protected void Show_Items_Click(object sender, EventArgs e)
    {
        Response.Redirect("ShowItems.aspx");
    }
    protected void add_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddItem.aspx");
    }
    protected void Home_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
    protected void Orders_Click(object sender, EventArgs e)
    {
        Response.Redirect("ShowOrders.aspx");
    }
}