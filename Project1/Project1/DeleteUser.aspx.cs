﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;

public partial class DeleteUser : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null)
            Response.Redirect("nopermission.aspx");
    }
    protected void Delete_Click(object sender, EventArgs e)
    {
        OleDbConnection con1 = new OleDbConnection();
        con1.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
        con1.Open();
        string sqlstring = "SELECT * FROM MyUsers where MyEmail ='" + Em.Text + "'";
        OleDbCommand conSer = new OleDbCommand(sqlstring, con1);
        OleDbDataReader Drdr = conSer.ExecuteReader();
        Drdr.Read();
        if (Drdr.HasRows)
        {
            sqlstring = "DELETE FROM MyUsers where MyEmail ='" + Em.Text + "'";
            conSer = new OleDbCommand(sqlstring, con1);
            Drdr = conSer.ExecuteReader();
            Drdr.Read();
            Response.Write(Em.Text + " נמחק בהצלחה");
        }
        else
        {
            Response.Write(Em.Text + "לא קיים משתמש כזה ");
        }
        con1.Close();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}