﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowOrders.aspx.cs" Inherits="ShowOrders" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body >
    <form id="form1" runat="server">
        <center>
        <div>
            <asp:Button ID="Button1" runat="server" Text="Show Filled Orders Only" BackColor="Teal" ForeColor="White" OnClick="Button1_Click" ></asp:Button>
            <asp:Button ID="Button2" runat="server" Text="Show All Orders" BackColor="Teal" ForeColor="White" OnClick="Button2_Click" ></asp:Button>
            <asp:Button ID="Button3" runat="server" Text="Show unfilled Orders Only" BackColor="Teal" ForeColor="White" OnClick="Button3_Click" ></asp:Button>


            <table border="1">
                    <tr>
                        <td style="background-color:green; height: 18px;"><asp:Label ID="Label1" runat="server" Text="OrderID" ForeColor="White"></asp:Label> </td>
                        <td style="background-color:red; height: 18px;"><asp:Label ID="Label2" runat="server" Text="ItemName" ForeColor="White"></asp:Label> </td>
                        <td style="background-color:green; height: 18px;"><asp:Label ID="Label3" runat="server" Text="OrderEmail" ForeColor="White"></asp:Label> </td>
                        <td style="background-color:red; height: 18px;"><asp:Label ID="Label4" runat="server" Text="Full Name" ForeColor="White"></asp:Label> </td>
                        <td style="background-color:green; height: 18px;"><asp:Label ID="Label5" runat="server" Text="CC Number" ForeColor="White"></asp:Label> </td>
                        <td style="background-color:red; height: 18px;"><asp:Label ID="Label6" runat="server" Text="CC Date" ForeColor="White"></asp:Label> </td>
                        <td style="background-color:green; height: 18px;"><asp:Label ID="Label7" runat="server" Text="CVC" ForeColor="White"></asp:Label> </td>
                        <td style="background-color:red; height: 18px;"><asp:Label ID="Label8" runat="server" Text="Address" ForeColor="White"></asp:Label> </td>
                        <td style="background-color:green; height: 18px;"><asp:Label ID="Label9" runat="server" Text="Country" ForeColor="White"></asp:Label> </td>
                        <td style="background-color:red; height: 18px;"><asp:Label ID="Label10" runat="server" Text="Postal Code" ForeColor="White"></asp:Label> </td>
                        <td style="background-color:green; height: 18px;"><asp:Label ID="Label11" runat="server" Text="City" ForeColor="White"></asp:Label> </td>
                        <td style="background-color:red; height: 18px;"><asp:Label ID="Label12" runat="server" Text="Price" ForeColor="White"></asp:Label> </td>
                        <td style="background-color:green; height: 18px;"><asp:Label ID="Label13" runat="server" Text="Supplier Email" ForeColor="White"></asp:Label> </td>
                        <td style="background-color:red; height: 18px;"><asp:Label ID="Label14" runat="server" Text="Status (filled or not)" ForeColor="White"></asp:Label> </td>

                    </tr>
                <asp:Repeater ID="Repeaterofusers" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%#DataBinder.Eval(Container.DataItem, "OrderID") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "ItemName") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "OrderEmail") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "FullName") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "CCNumber") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "CCDate") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "3Digits") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "Address") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "Country") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "Postal") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "City") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "Price") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "SupplierEmail") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "Filled") %></td>
                                    <td><a href="FilledUpdate.aspx?ID=<%#DataBinder.Eval(Container.DataItem, "OrderID")%>&Filled=<%#DataBinder.Eval(Container.DataItem, "Filled") %>"><asp:Label ID="Label8" runat="server" Text="Change Status" ForeColor="LimeGreen"></asp:Label></a></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
            </table>
            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" Text="Go Back">Homepage</asp:LinkButton>
        </div>
            </center>
    </form>
</body>
</html>