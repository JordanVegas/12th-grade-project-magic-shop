﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Item
/// </summary>
public class Item
{

    private string itemCode;
    public string ItemCode
    {
        get { return itemCode; }
        set { itemCode = value; }
    }
}
