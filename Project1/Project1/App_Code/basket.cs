﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;

/// <summary>
/// Summary description for basket
/// </summary>
public class basket
{
    private ArrayList _basket = new ArrayList();
    public ArrayList Basket
    {
        get { return _basket; }
        set { _basket = value; }
    }
    public void addItem(Item purchese)
    {
        _basket.Add(purchese);
    }
}
