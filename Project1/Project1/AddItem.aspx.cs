﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Web.Caching;
using System.IO;


public partial class AddItem : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null && Session["seller"] == null)
            Response.Redirect("nopermission.aspx");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        bool flag = false;
        itemname.Text.Replace("'", "''");
        description.Text = description.Text.Replace("'", "''");
        price.Text.Replace("'", "''");
        category.Text.Replace("'", "''");
        creator.Text.Replace("'", "''");
        Uses.Text.Replace("'", "''");


        string strFileName;
        string strFilePath;
        string strFolder;
        strFolder = Server.MapPath("./images/");
        // Retrieve the name of the file that is posted.
        strFileName = picupload.PostedFile.FileName;
        strFileName = Path.GetFileName(strFileName);
        if (picupload.Value != "")
        {
            // Create the folder if it does not exist.
            if (!Directory.Exists(strFolder))
            {
                Directory.CreateDirectory(strFolder);
            }
            // Save the uploaded file to the server.
            strFilePath = strFolder + strFileName;
            if (File.Exists(strFilePath))
            {
                Response.Write(strFileName + " already exists on the server!");
            }
            else
            {
                picupload.PostedFile.SaveAs(strFilePath);
                Response.Write(strFileName + " has been successfully uploaded.");
                flag = true;
            }
        }



        if (flag)
        {
            OleDbConnection con1 = new OleDbConnection();
            con1.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
            con1.Open();
            string sqlstring = "insert into MyItems (ItemName,Description,Price,Category,Creator,Uses,Email,ItemPic,Stock) values ('" + itemname.Text + "','" + description.Text + "','" + price.Text + "','" + category.Text + "','" + creator.Text + "','" + Uses.Text + "','" + Session["user"].ToString() + "','" + "images\\" + strFileName +"','" + Stock.Text + "')";
            OleDbCommand conSer = new OleDbCommand(sqlstring, con1);
            int Check = 0;
            Check = conSer.ExecuteNonQuery();
            con1.Close();
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        itemname.Text = "";
        description.Text = "";
        price.Text = "";
        category.Text = "";
        creator.Text = "";
        Uses.Text = "";
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
}