﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;

public partial class ShowBasket : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] == null)
            Response.Redirect("nopermission.aspx");
        if (Session["basket"] != null){
           
            
            basket b = new basket();
            b = (basket)Session["basket"];
            DataTable DT = new DataTable();

            DataColumn Oname = new DataColumn("ItemName");
            DataColumn Ocategory = new DataColumn("Category");
            DataColumn Oprice = new DataColumn("ItemPrice");
            DataColumn OpricePounds = new DataColumn("ItemPriceP");
            DataColumn Ocode = new DataColumn("ItemCode");
            DataColumn Opic = new DataColumn("ItemPic");
            DT.Columns.Add(Oname);
            DT.Columns.Add(Ocategory);
            DT.Columns.Add(Oprice);
            DT.Columns.Add(OpricePounds);
            DT.Columns.Add(Ocode);
            DT.Columns.Add(Opic);


            OleDbConnection Con = new OleDbConnection();
            Con.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
            Con.Open();



            int sum = 0;


            foreach (Item i in b.Basket)
            {
                string sqlstring = "select * from MyItems where Code=" + i.ItemCode + "";
                OleDbCommand Cmd = new OleDbCommand(sqlstring, Con);
                OleDbDataReader Dr = Cmd.ExecuteReader();

                Dr.Read();


                DataRow DD = DT.NewRow();
                DD["ItemName"] = Dr["ItemName"].ToString();
                DD["Category"] = Dr["Category"].ToString();
                DD["ItemPrice"] = Dr["Price"].ToString();
                DD["ItemCode"] = Dr["Code"].ToString();
                DD["ItemPic"] = Dr["ItemPic"].ToString();
                sum += int.Parse(Dr["Price"].ToString());


                DT.Rows.Add(DD);

                DataList1.DataSource = DT;
                DataList1.DataBind();


                Session["DL"] = DataList1;
            }
            Response.Write("summary:" + "₪ " + sum);
            localhost.WebService item = new localhost.WebService();
            double Price = item.ILStoEUR(sum);
            Response.Write(" or € " + Math.Truncate(Price * 100)/100);
        }
        else
        {
            Response.Write("Your Cart is Empty");
        }
    }

    protected void Buy_Click(object sender, EventArgs e)
    {
        Response.Redirect("Order.aspx");
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("default.aspx");
    }
}