﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdateUser.aspx.cs" Inherits="UpdateUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body >
    <form id="form1" runat="server">
        <center>
        <div>
             <table>
        <tr>
            <td>
 <asp:Label ID="Label1" runat="server" Text="User's Email"></asp:Label>

            </td>
            
        </tr>
         <tr>
            <td class="auto-style1">
 
 <asp:TextBox ID="Em" runat="server" ></asp:TextBox>

                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="Em"></asp:RequiredFieldValidator>

            </td>

        </tr>
         <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="First Name"></asp:Label>
              
            </td>
        </tr>
                 <tr>
                     <td>
                         <asp:TextBox ID="Fn" runat="server" ></asp:TextBox>
                     </td>

                 </tr>
                 <tr>
                     <td>
                         <asp:Label ID="Label3" runat="server" Text="Last Name"></asp:Label>
                     </td>

                 </tr>
                 <tr>
                     <td>
                         <asp:TextBox ID="Ln" runat="server" ></asp:TextBox>
                       </td>
                 </tr>
                  <tr>
                     <td>
                         <asp:Label ID="Label4" runat="server" Text="Admin:"></asp:Label>
                     </td>

                 </tr><tr>
                      <td>
                 <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem>Yes
                    </asp:ListItem>
                    <asp:ListItem>No
                    </asp:ListItem>
                     </asp:RadioButtonList></td>
</tr>
                                   <tr>
                     <td>
                         <asp:Label ID="Label5" runat="server" Text="Seller:"></asp:Label>
                     </td>

                 </tr><tr>
                      <td>
                 <asp:RadioButtonList ID="RadioButtonList4" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem>Yes
                    </asp:ListItem>
                    <asp:ListItem>No
                    </asp:ListItem>
                     </asp:RadioButtonList></td>
</tr>

         <tr>
            <td>
<asp:Button ID="Update" runat="server" Text="Update" OnClick="Update_Click" />

            </td>
             

        </tr>

    </table>
        
        </div>
             <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" Text="Go Back" CausesValidation="False">Homepage</asp:LinkButton>
    </center>
            </form>
</body>
</html>
