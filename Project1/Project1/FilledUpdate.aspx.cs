﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;

public partial class FilledUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string id = Request.QueryString["ID"].ToString();

        OleDbConnection con1 = new OleDbConnection();
        con1.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
        con1.Open();
        string sqlstring;
        if (Request.QueryString["Filled"].ToString() == "Yes")
        {
            sqlstring = "UPDATE MyOrders SET Filled='" + "No" + "' where OrderID=" + id + "";
        }
        else
        {
            sqlstring = "UPDATE MyOrders SET Filled='" + "Yes" + "' where OrderID=" + id + "";
        }

        OleDbCommand conSer = new OleDbCommand(sqlstring, con1);
        conSer.ExecuteNonQuery();
        con1.Close();
        Response.Redirect("ShowOrders.aspx");
        
    }
}