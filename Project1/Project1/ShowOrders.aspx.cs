﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;

public partial class ShowOrders : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null && Session["seller"] == null)
            Response.Redirect("nopermission.aspx");
        string filter = "1=1";
        if (Session["seller"] != null)
        {
            filter = "SupplierEmail='" + Session["Seller"].ToString() + "' ";
        }

        if (!IsPostBack)
        {
            OleDbConnection con1 = new OleDbConnection();
            con1.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
            con1.Open();
            string sqlstring = "select * FROM MyOrders " + "where " + filter + " Order By Address asc";
            OleDbCommand conSer = new OleDbCommand(sqlstring, con1);
            OleDbDataReader Drdr = conSer.ExecuteReader();
            Repeaterofusers.DataSource = Drdr;
            Repeaterofusers.DataBind();
            con1.Close();
        }
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        string filter = "1=1";
        if (Session["seller"] != null)
        {
            filter = "SupplierEmail='" + Session["Seller"].ToString() + "' ";
        }

        OleDbConnection con1 = new OleDbConnection();
        con1.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
        con1.Open();
        string sqlstring = "select * FROM MyOrders where Filled='Yes' " + "and " + filter + " Order By Address asc";
        OleDbCommand conSer = new OleDbCommand(sqlstring, con1);
        OleDbDataReader Drdr = conSer.ExecuteReader();
        Repeaterofusers.DataSource = Drdr;
        Repeaterofusers.DataBind();
        con1.Close();
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        string filter = "1=1";
        if (Session["seller"] != null)
        {
            filter = "SupplierEmail='" + Session["Seller"].ToString() + "' ";
        }
        OleDbConnection con1 = new OleDbConnection();
        con1.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
        con1.Open();
        string sqlstring = "select * FROM MyOrders " + "where " + filter + " Order By Address asc";
        OleDbCommand conSer = new OleDbCommand(sqlstring, con1);
        OleDbDataReader Drdr = conSer.ExecuteReader();
        Repeaterofusers.DataSource = Drdr;
        Repeaterofusers.DataBind();
        con1.Close();
    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        string filter = "1=1";
        if (Session["seller"] != null)
        {
            filter = "SupplierEmail='" + Session["Seller"].ToString() + "' ";
        }
        OleDbConnection con1 = new OleDbConnection();
        con1.ConnectionString = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = " + Server.MapPath("") + "\\MyDataBase.accdb";
        con1.Open();
        string sqlstring = "select * FROM MyOrders where Filled='No' " + "and " + filter + " Order By Address asc";
        OleDbCommand conSer = new OleDbCommand(sqlstring, con1);
        OleDbDataReader Drdr = conSer.ExecuteReader();
        Repeaterofusers.DataSource = Drdr;
        Repeaterofusers.DataBind();
        con1.Close();
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }

}