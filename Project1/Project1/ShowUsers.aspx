﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowUsers.aspx.cs" Inherits="ShowUsers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body >
    <form id="form1" runat="server">
        <center>
        <div>
            <table border="1">
                    <tr>
                    <td style="background-color:gainsboro; height: 18px;"><asp:Label ID="Label1" runat="server" Text="First Name" ForeColor="Black"></asp:Label> </td>
                    <td style="background-color:red; height: 18px;"><asp:Label ID="Label2" runat="server" Text="Last Name" ForeColor="White"></asp:Label> </td>
                    <td style="background-color:gainsboro; height: 18px;"><asp:Label ID="Label3" runat="server" Text="Email" ForeColor="Black"></asp:Label> </td>
                    <td style="background-color:red; height: 18px;"><asp:Label ID="Label4" runat="server" Text="Phone" ForeColor="White"></asp:Label> </td>
                    <td style="background-color:gainsboro; height: 18px;"><asp:Label ID="Label5" runat="server" Text="Password" ForeColor="Black"></asp:Label> </td>
                    <td style="background-color:red; height: 18px;"><asp:Label ID="Label6" runat="server" Text="Gender" ForeColor="White"></asp:Label> </td>
                    </tr>
                <asp:Repeater ID="Repeaterofusers" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%#DataBinder.Eval(Container.DataItem, "Myfirstname") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "Mylastname") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "MyEmail") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "PhoneNum") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "MyPassword") %></td>
                                    <td><%#DataBinder.Eval(Container.DataItem, "Gender") %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
            </table>
            <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" Text="Go Back">Homepage</asp:LinkButton>
        </div>
            </center>
    </form>
</body>
</html>
