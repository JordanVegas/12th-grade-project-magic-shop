﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Items.aspx.cs" Inherits="Items" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Gallery</title>
    <style type="text/css">
        .auto-style1 {
            height: 26px;
            width: 1399px;
        }
        .auto-style2 {
            width: 1399px;
        }
        .auto-style3 {
            width: 111%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <center>
        <asp:Label ID="Lheader" runat="server" Text="Items" Font-Bold="True" Font-Size="25"></asp:Label></center>
<table><tr ><td style="auto-style2;">
        <asp:Button ID="Button2" runat="server" Text="Cards" BackColor="Teal" ForeColor="White" OnClick="Button2_Click" ></asp:Button>
    <asp:Button ID="Button1" runat="server" Text="ITR" BackColor="Teal" ForeColor="White" OnClick="Button1_Click"></asp:Button>
    <asp:Button ID="Button3" runat="server" Text="Gimmicks" BackColor="Teal" ForeColor="White" OnClick="Button3_Click"  ></asp:Button>
    <asp:Button ID="Button4" runat="server" Text="General" BackColor="Teal" ForeColor="White" OnClick="Button4_Click" ></asp:Button>
    <asp:Button ID="Button20" runat="server" Text="All" BackColor="Teal" ForeColor="White" OnClick="Button20_Click" ></asp:Button>

              </td></tr></table><table style="border-spacing:1px">
    <tr>
        <td  width="60px" style="background-color:#E61818;"><asp:Label ID="Label9" runat="server" Text="ID" ForeColor="White"></asp:Label></td>  
        <td width="205px" style="background-color:#E61818;"><asp:Label ID="table1" runat="server" Text="Picture" ForeColor="White"></asp:Label></td>  
        <td width="150px" style="background-color:#E61818;"><asp:Label ID="Label2" runat="server" Text="Item Name" ForeColor="White"></asp:Label></td>
        <td width="90px" style="background-color:#E61818;"><asp:Label ID="Label3" runat="server" Text="Category" ForeColor="White"></asp:Label></td>
        <td width="60px" style="background-color:#E61818;"><asp:Label ID="Label4" runat="server" Text="Creator" ForeColor="White"></asp:Label></td>
        <td width="150px" style="background-color:#E61818;"><asp:Label ID="Label6" runat="server" Text="Email" ForeColor="White"></asp:Label></td>
        <td width="90px" style="background-color:#E61818;"><asp:Label ID="Label5" runat="server" Text="Price" ForeColor="White"></asp:Label></td>
            
        </tr></table>
        <asp:DataList ID="Shop" runat="server"  DataKeyField="Code" Style="color:darkkhaki;" BorderColor="Black" GridLines="Both" OnItemCommand="shop_ItemCommand" OnSelectedIndexChanged="Shop_SelectedIndexChanged">
            <ItemTemplate>

                <tr>
                <td  width="60px"><asp:Label ID="Label8" runat="server" Font-Bold="true" Font-Size="1.2em" Text=""><%#DataBinder.Eval(Container.DataItem,"Code" )%></asp:Label></td>
                <td width="205px"><span title="Picture"><asp:Image ID="Image1" runat="server" Height="200" Width="200" ImageUrl=<%#DataBinder.Eval(Container.DataItem,"ItemPic" )%> ></asp:Image></span></td>
                <td width="150px"><asp:Label ID="Llocation2" runat="server" Font-Bold="true" Font-Size="1.2em" Text=""><%#DataBinder.Eval(Container.DataItem,"ItemName" )%></asp:Label></td>
                <td width="90px"><asp:Label ID="Ldestenation2" runat="server" ForeColor="Blue" Font-Bold="true" Font-Size="1.2em" Text=""><%#DataBinder.Eval(Container.DataItem,"Category")%></asp:Label></td>
                <td width="60px"><asp:Label ID="moreinfo" runat="server" ForeColor="Blue" Font-Bold="true" Font-Underline="false"  Text=""><%#DataBinder.Eval(Container.DataItem,"Creator")%></asp:Label></td>
                <td width="150px"><asp:Label ID="Label7" runat="server" ForeColor="Blue" Font-Bold="true" Font-Underline="false"  Text=""><%#DataBinder.Eval(Container.DataItem,"Email")%></asp:Label></td>
                <td width="90px"><asp:Label ID="Label1" runat="server" ForeColor="Blue" Font-Bold="true" Font-Underline="false"  Text=""><%#DataBinder.Eval(Container.DataItem,"Price")%>&nbsp ILS</asp:Label></td>
                  <td><asp:Button ID="Button4" runat="server" Text="add to basket" CommandName="AddToBasket" /></td>   </tr>
                

                            </ItemTemplate>
            </asp:DataList>
                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" CausesValidation="False">Homepage</asp:LinkButton>
                        <asp:LinkButton ID="LinkButton2" runat="server" OnClick="Basket_Click" CausesValidation="False">Show Basket</asp:LinkButton>

     


    </form>
</body>
</html>