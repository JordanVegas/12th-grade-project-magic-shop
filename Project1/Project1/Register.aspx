﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body >
    <form id="form1" runat="server">
        <center>
    <div>
        
    <table style="border-spacing: 5px;">
        <tr>
            <td>
 <asp:Label ID="Label1" runat="server" Text="First Name"></asp:Label>
                
            </td>
            
        </tr>
         <tr>
            <td>
 
 <asp:TextBox ID="fn" runat="server" ></asp:TextBox> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="fn" ErrorMessage="First Name Is Missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>

        </tr>
         <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Last Name"></asp:Label>
            </td>

        </tr>
         <tr>
            <td>
 <asp:TextBox ID="ln" runat="server" ></asp:TextBox> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ln" ErrorMessage="Last Name Is Missing" ForeColor="Red"></asp:RequiredFieldValidator>
               

            </td>

        </tr>
        <tr>
            <td>
                 <asp:Label ID="Label5" runat="server" Text="Phone"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="Tel" runat="server"> </asp:TextBox>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Tel" ErrorMessage="Phone is Missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr>
            <td>
                 <asp:Label ID="Label4" runat="server" Text="Email"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="Em" runat="server"> </asp:TextBox>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="Em" ErrorMessage="Email is missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Password"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
 <asp:TextBox ID="Pass" type="Password" runat="server"> </asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="Pass" ErrorMessage="Password is missing" ForeColor="Red"></asp:RequiredFieldValidator>
              

            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="confirmation Password"></asp:Label>
            </td>
            </tr>
        <tr>
            <td>
<asp:TextBox ID="RePass" type="Password" runat="server"> </asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="RePass" ErrorMessage="Password Confirmation is missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        
        
                   <tr>
                    <td>Gender:  <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem>Male
                    </asp:ListItem>
                    <asp:ListItem>Female
                    </asp:ListItem>
                    <asp:ListItem>Other
                    </asp:ListItem>
                       
                </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="RadioButtonList2" ErrorMessage="Gender is Missing" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
         <tr>
            <td>
<asp:Button ID="Button1" runat="server" Text="Register" OnClick="Button1_Click" />
&nbsp
                 <asp:Button ID="Button2" runat="server" Text="Clear" OnClick="Button2_Click" CausesValidation="False"/>
             </td>
             
        </tr>
         
    </table>
    </div>
       
        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" CausesValidation="False">Homepage</asp:LinkButton>
    </center>
            </form>
</body>
</html>
