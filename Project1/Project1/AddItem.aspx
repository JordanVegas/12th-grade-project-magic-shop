﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddItem.aspx.cs" Inherits="AddItem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body >
    <form id="form2" runat="server">
        <center>
    <div>
        
    <table style="border-spacing: 5px;">
        <tr>
            <td>
 <asp:Label ID="Label1" runat="server" Text="Item Name"></asp:Label>
                
            </td>
        </tr>
         <tr>
            <td>
 
 <asp:TextBox ID="itemname" runat="server" ></asp:TextBox> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="itemname" ErrorMessage="Item name is missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>

        </tr>
         <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Description"></asp:Label>
            </td>

        </tr>
         <tr>
            <td>
 <asp:TextBox ID="description" runat="server" ></asp:TextBox> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="description" ErrorMessage="Description is missing" ForeColor="Red"></asp:RequiredFieldValidator>
               

            </td>

        </tr>
        <tr>
            <td>
                <asp:Label ID="Label8" runat="server" Text="Item Pic:"></asp:Label>
            </td>

        </tr>
        <tr>
            <td>
                <input id="picupload" type="file" runat="server" NAME="picupload"><br />
                <asp:Label ID="Label7" runat="server" Text="price (in Shekels)"></asp:Label>
            </td>

        </tr>
         <tr>
            <td>
 <asp:TextBox ID="price" runat="server" ></asp:TextBox> 
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="price" ErrorMessage="price is missing" ForeColor="Red"></asp:RequiredFieldValidator>
               

            </td>

        </tr>
        <tr>
            <td>
                 <asp:Label ID="Label5" runat="server" Text="category"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="category" runat="server"> </asp:TextBox>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="category" ErrorMessage="category is missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr>
            <td>
                 <asp:Label ID="Label4" runat="server" Text="Creator"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="creator" runat="server"> </asp:TextBox>
               <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="creator" ErrorMessage="creator is missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>

        </tr>
        <tr>
            <td>
                <asp:Label ID="Label3" runat="server" Text="Uses number"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
 <asp:TextBox ID="Uses" runat="server"> </asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="Uses" ErrorMessage="Uses number is missing" ForeColor="Red"></asp:RequiredFieldValidator>
              

            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" Text="Stock"></asp:Label>
            </td>
            </tr>
        <tr>
            <td>
<asp:TextBox ID="Stock" runat="server"> </asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="Stock" ErrorMessage="stock is missing" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        
         <tr>
            <td>
<asp:Button ID="Button1" runat="server" Text="Register" OnClick="Button1_Click" />
&nbsp
                 <asp:Button ID="Button2" runat="server" Text="Clear" OnClick="Button2_Click" CausesValidation="False"/>
             </td>
             
        </tr>
         
    </table>
    </div>
       
        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" CausesValidation="False">HomePage</asp:LinkButton>
    </center>
            </form>
</body>
</html>
